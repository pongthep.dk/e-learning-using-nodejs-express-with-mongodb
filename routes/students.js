var express = require('express');
var router = express.Router();
var Student = require('../models/students');
var Classes = require('../models/classes');

router.get('/classes', (req, res, next) => {
    Student.getStudentByUserName(req.user.username, (err, student) => {
        res.render('students/classes', {student: student});
    });
});

router.post('/classes/register', (req, res, next) => {
    var student_user = req.body.student_user;
    var class_id = req.body.class_id;
    var class_title = req.body.class_title;
    
    info = [];
    info["student_user"] = student_user;
    info["class_id"] = class_id;
    info["class_title"] = class_title;

    Student.register(info, (err, student) => {
        if (err) throw err;
    });
    res.location('/students/classes');
    res.redirect('/students/classes');
});

module.exports = router;