var express = require('express');
var router = express.Router();
var Classes = require('../models/classes');
var Instructor = require('../models/instructors');

router.post('/register', (req, res, next) => {
    var class_id = req.body.class_id;
    var class_name = req.body.class_name;
    var img_url = req.body.img_url;
    var description = req.body.description;
    var instructor = req.body.instructor;
    var newClass = new Classes({
        class_id: class_id,
        title: class_name,
        img_url: img_url,
        description: description,
        instructor: instructor
    });
    var info = [];
    info["instructor_user"] = req.user.username;
    info["class_id"] = class_id;
    info["class_title"] = class_name;

    Classes.saveNewClass(newClass, (err, result) => {
        if (err) throw err;
    });

    Instructor.register(info, (err, instructor) => {
        if (err) throw err;
    });

    res.location('/instructors/classes');
    res.redirect('/instructors/classes');
});

router.get('/:id/lession', (req, res, next) => {
    Classes.getClassID(req.params.id, (err, className) => {
        res.render('classes/viewlession', {className: className});
    });
});

router.get('/:id/lession/:lession_id', (req, res, next) => {
    Classes.getClassID(req.params.id, (err, className) => {
        var lession;
        for (let i = 0; i < className.lession.length; i++) {
            if (className.lession[i].lession_number == req.params.lession_id) {
                lession = className.lession[i];
            }
        }
        res.render('classes/lession', {className: className, lession: lession});
    });
});

module.exports = router;