var express = require('express');
var router = express.Router();
var Instructors = require('../models/instructors');
var Classes = require('../models/classes');

router.get('/classes', (req, res, next) => {
    Instructors.getInstructorByUserName(req.user.username, (err, instructor) => {
        res.render('instructors/classes', {instructor:instructor});
    });
    
});

router.get('/classes/:id/lession/new', (req, res, next) => {
    res.render('instructors/newlession', {class_id: req.params.id});
});

router.post('/classes/:id/lession/new', (req, res, next) => {
    info = [];
    info["class_id"] = req.params.id;
    info["lession_number"] = req.body.lession_number;
    info["lession_title"] = req.body.lession_title;
    info["lession_body"] = req.body.lession_body;
    Classes.addLession(info, (err, lession) => {
        if (err) throw err;
    });
    res.location("/instructors/classes");
    res.redirect("/instructors/classes");
});

module.exports = router;
