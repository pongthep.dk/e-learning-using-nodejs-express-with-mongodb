var express = require("express");
var router = express.Router();
var Users = require("../models/users");
var Students = require("../models/students");
var Instructors = require("../models/instructors");
var passport = require("passport");
var LocalStrategy = require("passport-local").Strategy;

const { check, validationResult } = require("express-validator");

router.get("/login", (req, res, next) => {
  res.render("users/login");
});

router.post('/login', passport.authenticate('local', {
  failureRedirect: '/users/login',
  failureFlash: true
}), (req, res) => {
  var userType = req.user.type;
  res.redirect('/'+userType+'s/classes');
});

passport.serializeUser((user, done) => {
  done(null, user.id);
});

passport.deserializeUser((id, done) => {
  Users.getUserById(id, (err, user) => {
    done(err, user);
  });
});

passport.use(new LocalStrategy((username, password, done) => {
  Users.getUserByUserName(username, (err, user) => {
    if (err) throw error
    if (!user) {
      return done(null, false);
    }
    return Users.comparePassword(password, user.password, (err, isMatch) => {
      if (err) throw error
      if (!isMatch) {
        return done(null, false);
      } else {
        return done(null, user); 
      }
    });
  });
}
));

/* GET users listing. */
router.get("/register", (req, res, next) => {
  res.render("users/register");
});

router.post(
  "/register",
  [
    check("username", "กรุณาป้อนชื้อผู้ใช้")
      .not()
      .isEmpty(),
    check("password", "กรุณาป้อนรหัสผ่าน")
      .not()
      .isEmpty(),
    check("email", "กรุณาป้อนอีเมล์").isEmail(),
    check("fname", "กรุณาป้อนชื่อของท่าน")
      .not()
      .isEmpty(),
    check("lname", "กรุณาป้อนนามสกุลของท่าน")
      .not()
      .isEmpty()
  ],
  (req, res, next) => {
    const result = validationResult(req);
    var errors = result.errors;
    //Validation Data
    if (!result.isEmpty()) {
      res.render("users/register", {
        errors: errors
      });
    } else {
      var type = req.body.type;
      var username = req.body.username;
      var password = req.body.password;
      var email = req.body.email;
      var fname = req.body.fname;
      var lname = req.body.lname;
      var newUser = new Users({
        username: username,
        email: email,
        password: password,
        type: type
      });
      if (type == "student") {
        var newStudent = new Students({
          username: username,
          fname: fname,
          lname: lname,
          email: email
        });
        Users.saveStudent(newUser, newStudent, (err, user) => {
          if (err) throw err;
        });
      } else {
        var newInstructors = new Instructors({
          username: username,
          fname: fname,
          lname: lname,
          email: email
        });
        Users.saveInstructor(newUser, newInstructors, (err, user) => {
          if (err) throw err;
        });
      }
      res.redirect("/");
    }
  }
);

router.get('/logout', (req, res) => {
  req.logout();
  res.redirect('/users/login');
});

module.exports = router;
