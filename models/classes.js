var mongoose = require("mongoose");

var mongoDB = "mongodb://localhost:27017/ElearningDB";

mongoose.connect(mongoDB, {
  useNewUrlParser: true
});

//Connect
var db = mongoose.connection;
db.on("error", console.error.bind(console, "Mongodb Connect Error"));

var classesSchema = mongoose.Schema({
  class_id: {
    type: String
  },
  title: {
    type: String
  },
  description: {
    type: String
  },
  img_url: {
    type: String
  },
  instructor: {
    type: String
  },
  lession: [
    {
      lession_number: {
        type: Number
      },
      lession_title: {
        type: String
      },
      lession_body: {
        type: String
      }
    }
  ]
});
var Classes = (module.exports = mongoose.model("classes", classesSchema));

module.exports.getClasses = (callback, limit) => {
  Classes.find(callback).limit(limit);
};

module.exports.getClassID = (class_id, callback) => {
    var query = {
        class_id: class_id
    }
    Classes.findOne(query, callback);
}

module.exports.saveNewClass = (newClass, callback) => {
  newClass.save(callback);
};

module.exports.addLession = (info, callback) => {
  lession_number = info["lession_number"];
  lession_title = info["lession_title"];
  lession_body = info["lession_body"];
  class_id = info["class_id"];
  var query = {
    class_id: class_id
  };
  Classes.findOneAndUpdate(
    query,
    {
      $push: {
        lession: {
          lession_number: lession_number,
          lession_title: lession_title,
          lession_body: lession_body
        }
      }
    },
    {
      safe: true,
      upsert: true
    },
    callback
  );
};
