var mongoose = require('mongoose');

var mongoDB = 'mongodb://localhost:27017/ElearningDB';
var bcrypt = require('bcryptjs');
mongoose.connect(mongoDB, {
    useNewUrlParser: true
});

//Connect
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'Mongodb Connect Error'));

var userSchema = mongoose.Schema({
    username:{
        type:String
    },
    email:{
        type:String
    },
    password:{
        type:String
    },
    type:{
        type:String
    }
});
var Users = module.exports = mongoose.model('users', userSchema);

module.exports.saveStudent = (newUser, newStudent, callback) => {
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
            newUser.password = hash;
            newUser.save(callback);
            newStudent.save(callback);
        });
    });
}

module.exports.saveInstructor = (newUser, newInstrutor, callback) => {
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
            newUser.password = hash;
            newUser.save(callback);
            newInstrutor.save(callback);
        });
    });
}


module.exports.getUserById = (id, callBack) => {
    Users.findById(id, callBack);
}

module.exports.getUserByUserName = (username, callBack) => {
    var query = {
        username: username,
    }
    Users.findOne(query, callBack);
}

module.exports.comparePassword = (password, hash, callBack) => {
    bcrypt.compare(password, hash, (err, isMatch) => {
        callBack(null, isMatch);
    });
}